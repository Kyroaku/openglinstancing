#include "openglitem.h"
#include "openglrenderer.h"

#include <QSGSimpleTextureNode>
#include <qmessagebox.h>

OpenGLItem::OpenGLItem()
{
    m_RenderTimeAquistTimer.setInterval(100);
    connect(&m_RenderTimeAquistTimer, &QTimer::timeout, [&]() {
        static int C = 0;
        if(m_Renderer == nullptr)
            return;
        emit this->renderTimeSignal(
                    C++,
                    m_Renderer->getRenderTimeStamp()
                    );
    });
    m_RenderTimeAquistTimer.start();
}

QQuickFramebufferObject::Renderer *OpenGLItem::createRenderer() const
{
    m_Renderer = new OpenGLRenderer();
    return static_cast<QQuickFramebufferObject::Renderer*>(m_Renderer);
}

QSGNode *OpenGLItem::updatePaintNode(QSGNode *node, QQuickItem::UpdatePaintNodeData *nodeData)
{
    if (!node) {
        node = QQuickFramebufferObject::updatePaintNode(node, nodeData);
        QSGSimpleTextureNode *n = static_cast<QSGSimpleTextureNode *>(node);
        if (n) {
            n->setTextureCoordinatesTransform(QSGSimpleTextureNode::MirrorVertically);
            node = QQuickFramebufferObject::updatePaintNode(node, nodeData);
        }
        return node;
    }
    return QQuickFramebufferObject::updatePaintNode(node, nodeData);
}

void OpenGLItem::setRenderCount(const int &count)
{
    if(m_Renderer == nullptr)
        return;
    m_Renderer->setRenderCount(count);
}

int OpenGLItem::renderCount() const
{
    if(m_Renderer == nullptr)
        return 0;
    return m_Renderer->renderCount();
}

void OpenGLItem::setRenderSize(const int &size)
{
    if(m_Renderer == nullptr)
        return;
    m_Renderer->setRenderSize(size);
}

int OpenGLItem::renderSize() const
{
    if(m_Renderer == nullptr)
        return 0;
    return m_Renderer->renderSize();
}

void OpenGLItem::setDepthFunc(const QString &func)
{
    if(m_Renderer == nullptr)
        return;
    m_Renderer->setDepthFunc(func);
}

QString OpenGLItem::depthFunc()
{
    if(m_Renderer == nullptr)
        return "";
    return m_Renderer->depthFunc();
}

void OpenGLItem::setNumObjectVertices(const int &numVerts)
{
    if(m_Renderer == nullptr)
        return;
    m_Renderer->setNumObjectVertices(size_t(numVerts));
}

int OpenGLItem::numObjectVertices()
{
    if(m_Renderer == nullptr)
        return 0;
    return int(m_Renderer->numObjectVertices());
}

void OpenGLItem::setRenderMultipleBatches(bool state)
{
    if(m_Renderer == nullptr)
        return;
    m_Renderer->setRenderMultipleBatches(state);
}

bool OpenGLItem::renderMultipleBatches()
{
    if(m_Renderer == nullptr)
        return false;
    return m_Renderer->renderMultipleBatches();
}

void OpenGLItem::generateCharacteristic(int from, int to, int step)
{
    /* Check if file is writable */
    QFile excelFile("Characteristics.xlsx");
    if(!excelFile.open(QIODevice::ReadWrite)) {
        QMessageBox::warning(nullptr, "Characteristic", "Couldn't open file 'Characteristics.xlsx'. It is already open or it \
                                        is unwritable.");
    }
    else {
        m_Renderer->generateCharacteristic(from, to, step);
    }
}
