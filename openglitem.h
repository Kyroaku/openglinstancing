#ifndef OPENGLITEM_H
#define OPENGLITEM_H

#include "openglrenderer.h"

#include <qquickframebufferobject.h>
#include <qtimer.h>



class OpenGLItem : public QQuickFramebufferObject
{
    Q_OBJECT
    Q_PROPERTY(int renderCount READ renderCount WRITE setRenderCount)
    Q_PROPERTY(int renderSize READ renderSize WRITE setRenderSize)
    Q_PROPERTY(QString depthFunc READ depthFunc WRITE setDepthFunc)
    Q_PROPERTY(int numObjectVertices READ numObjectVertices WRITE setNumObjectVertices)
    Q_PROPERTY(bool renderMultipleBatches READ renderMultipleBatches WRITE setRenderMultipleBatches)

private:
    mutable OpenGLRenderer *m_Renderer = nullptr;
    QTimer m_RenderTimeAquistTimer;

public:
    OpenGLItem();

    QQuickFramebufferObject::Renderer *createRenderer() const;

    QSGNode *updatePaintNode(QSGNode *node, QQuickItem::UpdatePaintNodeData *nodeData);

    void setRenderCount(const int &count);
    int renderCount() const;

    void setRenderSize(const int &count);
    int renderSize() const;

    void setDepthFunc(const QString &func);
    QString depthFunc();

    void setNumObjectVertices(const int &numVerts);
    int numObjectVertices();

    void setRenderMultipleBatches(bool state);
    bool renderMultipleBatches();

public slots:
    void generateCharacteristic(int from, int to, int step);

signals:
    void renderTimeSignal(float timeStamp, QVector4D renderTime);
};

#endif // OPENGLITEM_H
