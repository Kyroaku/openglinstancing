import QtQuick 2.0
import QtQuick.Window 2.0
import QtCharts 2.3

import openglitem 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12

Window {
    id: window
    width: 1280
    height: 768
    visible: true
    title: qsTr("Hello World")

    Item {
        id: openGLView
        x: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: sideBar.right
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.bottom: chart.top

        OpenGLItem {
            id: openglitem
            anchors.fill: parent
            property var x_mean: 0
            property var y_mean: 0
            onRenderTimeSignal: {
                if(axisY.max < renderTime.x)
                    axisY.max = renderTime.x * 1.1
                if(axisY.max < renderTime.y)
                    axisY.max = renderTime.y * 1.1
                if(axisY.max < renderTime.z)
                    axisY.max = renderTime.z * 1.1
                if(axisY.max < renderTime.w)
                    axisY.max = renderTime.w * 1.1
                chart.scrollRight(chart.mapToPosition(Qt.point(timeStamp,0), chart.series(0)).x - chart.width/1.1);
                chart.series(0).append(timeStamp, renderTime.x)
                chart.series(1).append(timeStamp, renderTime.y)
                chart.series(2).append(timeStamp, renderTime.z)
            }
        }
    }

    SideBar {
        id: sideBar
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        title: "Settings"
        wheelEnabled: false
        width: 300
    }

    ChartView {
        id: chart
        y: 548
        height: 329
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: sideBar.right
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        Layout.fillHeight: true
        Layout.fillWidth: true
        localizeNumbers: true
        antialiasing: true
        title: "Time spent on rendering"
        theme: ChartView.ChartThemeQt

        function updateTitle(value) {
            title = "Time spent on rendering " + value.toString() + " triangles"
        }

        ValueAxis {
            id: axisX
            min: 0
            max: 30
            tickCount: 5
        }

        ValueAxis {
            id: axisY
            min: 0
            max: 0.001
        }

        LineSeries {
            name: 'single call'
            axisX: axisX
            axisY: axisY
            color: "#FF0000CC"
            width: 2
        }

        LineSeries {
            name: 'instancing'
            axisX: axisX
            axisY: axisY
            color: "#FF00CC00"
            width: 2
        }

        LineSeries {
            id: multipleBatchesSeries
            name: 'multiple calls'
            axisX: axisX
            axisY: axisY
            color: "#FFCC0000"
            visible: false
            width: 2
        }
    }
}





/*##^##
Designer {
    D{i:1;anchors_height:0;anchors_y:40}D{i:3;anchors_height:200}D{i:6;anchors_height:28;anchors_width:100;anchors_x:0;anchors_y:62}
}
##^##*/
