#ifndef RENDERMODEL_H
#define RENDERMODEL_H

#include <memory>
#include <qopenglvertexarrayobject.h>

class QOpenGLExtraFunctions;

class RenderModel
{
public:
    const size_t MAX_OBJECTS = 1000000;
    const size_t NUM_VERTICES_PER_OBJECT;

private:
    GLuint m_VBO_pos, m_VBO_pos_inst;
    GLuint m_VBO_colors, m_VBO_colors_inst;
    GLuint m_VBO_verts, m_VBO_verts_inst;\
    std::unique_ptr<QOpenGLVertexArrayObject> m_VAO = nullptr;
    std::unique_ptr<QOpenGLVertexArrayObject> m_VAO_inst = nullptr;
    GLenum m_RenderMode = GL_TRIANGLES;

public:
    RenderModel(const size_t &numVertices);
    ~RenderModel();

    void renderMultipleBatches(QOpenGLExtraFunctions *ogl, int numObjects);
    void renderSingleBatch(QOpenGLExtraFunctions *ogl, int numObjects);
    void renderInstanced(QOpenGLExtraFunctions *ogl, int numObjects);

private:
    void generateData(QOpenGLExtraFunctions *ogl);
};

#endif // RENDERMODEL_H
