import QtQuick 2.0
import QtQuick.Window 2.0
import QtCharts 2.3

import openglitem 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12

Page {
    id: page
    Label {
        id: objectsCountLabel
        x: 0
        y: 23
        width: parent.width
        text: qsTr("Objects count")
        leftPadding: 8
        anchors.left: parent.left
        verticalAlignment: Text.AlignTop
        font.bold: false
        anchors.top: objectsTypeCombo.bottom
        font.pointSize: 10
        font.weight: Font.Normal
        horizontalAlignment: Text.AlignLeft
        font.family: "Verdana"
        anchors.leftMargin: 0
        fontSizeMode: Text.Fit
        font.italic: false
    }
    
    Label {
        id: renderSettingsLabel
        width: parent.width
        text: qsTr("Rendering")
        font.weight: Font.Normal
        font.italic: false
        font.bold: true
        font.pointSize: 14
        font.family: "Verdana"
        fontSizeMode: Text.Fit
        verticalAlignment: Text.AlignTop
        horizontalAlignment: Text.AlignHCenter
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
    }
    
    RowLayout {
        id: renderCountButtons
        x: 0
        y: 67
        width: parent.width
        height: 28
        anchors.top: renderCountSpin.bottom
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        spacing: 0
        
        
        
        Button {
            id: renderCountMinButton
            text: qsTr("Min")
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
        
        
        
        Button {
            id: renderCountMedButton
            text: qsTr("Med")
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
        
        Button {
            id: renderCountMaxButton
            text: qsTr("Max")
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
    
    SpinBox {
        id: renderCountSpin
        x: 0
        y: 39
        width: parent.width
        height: 28
        anchors.top: objectsCountLabel.bottom
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        leftPadding: 46
        value: 3
        wheelEnabled: true
        stepSize: 10000
        to: 1000000
        from: 1
        
        onValueChanged: {
            openglitem.renderCount = value
        }
    }
    
    Label {
        id: characteristicLabel
        x: 2
        text: qsTr("Objects count range")
        anchors.top: charSettingsLabel.bottom
        anchors.right: parent.right
        anchors.rightMargin: 0
        fontSizeMode: Text.Fit
        font.pointSize: 10
        verticalAlignment: Text.AlignTop
        font.family: "Verdana"
        font.bold: false
        anchors.leftMargin: 0
        horizontalAlignment: Text.AlignLeft
        leftPadding: 8
        font.weight: Font.Normal
        font.italic: false
        anchors.left: parent.left
    }
    
    RangeSlider {
        id: characteristicRangeSlider
        stepSize: 10000
        to: 1000000
        from: 1
        anchors.right: parent.right
        anchors.top: characteristicLabel.bottom
        anchors.left: parent.left
        first.value: 10000
        second.value: 1000000
        
        Component.onCompleted: {
            charFirstText.text = first.value
            charSecondText.text = second.value
        }
        
        
        first.onMoved: {
            setValues(Math.floor(first.value), Math.floor(second.value))
            charFirstText.text = first.value
            charSecondText.text = second.value
        }
        second.onMoved: {
            setValues(Math.floor(first.value), Math.floor(second.value))
            charFirstText.text = first.value
            charSecondText.text = second.value
        }
    }
    
    Button {
        id: generateCharButton
        text: qsTr("Generate chracteristic")
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: charStepSpin.bottom
    }
    
    RowLayout {
        id: charRangeNumerics
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: characteristicRangeSlider.bottom
        
        TextArea {
            id: charFirstText
            width: 150
            text: qsTr("Text Area")
            Layout.maximumHeight: 25
            wrapMode: Text.NoWrap
            Layout.maximumWidth: 150
            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
        
        TextArea {
            id: charSecondText
            x: 150
            width: 150
            text: qsTr("Text Area")
            Layout.maximumHeight: 25
            Layout.maximumWidth: 150
            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
        
    }
    
    SpinBox {
        id: charStepSpin
        height: 25
        value: 10000
        wheelEnabled: true
        stepSize: 1000
        to: 100000
        from: 1
        anchors.leftMargin: 12
        anchors.right: parent.right
        anchors.left: charStepLabel.right
        anchors.top: charRangeNumerics.bottom
    }
    
    Label {
        id: charStepLabel
        text: qsTr("Step")
        anchors.right: parent.right
        anchors.rightMargin: 278
        horizontalAlignment: Text.AlignHCenter
        anchors.bottom: charStepSpin.bottom
        anchors.leftMargin: 8
        anchors.top: charStepSpin.top
        anchors.left: parent.left
        verticalAlignment: Text.AlignVCenter
    }
    
    Label {
        id: objectsSizeLabel
        x: 2
        text: qsTr("Objects size")
        anchors.right: parent.right
        anchors.top: renderCountButtons.bottom
        fontSizeMode: Text.Fit
        font.weight: Font.Normal
        horizontalAlignment: Text.AlignLeft
        leftPadding: 8
        verticalAlignment: Text.AlignTop
        font.italic: false
        font.bold: false
        font.family: "Verdana"
        anchors.left: parent.left
        font.pointSize: 10
    }
    
    SpinBox {
        id: renderSizeSpin
        x: -2
        height: 28
        anchors.right: parent.right
        anchors.rightMargin: 0
        stepSize: 1
        anchors.top: objectsSizeLabel.bottom
        anchors.leftMargin: 0
        value: 1
        to: 100
        leftPadding: 46
        from: 1
        anchors.left: parent.left
        wheelEnabled: true
    }
    
    ComboBox {
        id: depthFuncCombo
        height: 28
        textRole: "text"
        flat: false
        editable: false
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: depthFuncLabel.bottom
        currentIndex: 0
        model: ListModel {
            id: depthFuncItems
            ListElement { text: "Disabled"; glfunc: "DISABLED" }
            ListElement { text: "Less"; glfunc: "GL_LESS" }
            ListElement { text: "Less or equal"; glfunc: "GL_LEQUAL" }
        }
        
        Component.onCompleted: {
            openglitem.depthFunc = depthFuncItems.get(currentIndex).glfunc
        }
    }
    
    Label {
        id: depthFuncLabel
        x: -1
        text: qsTr("Depth test")
        verticalAlignment: Text.AlignTop
        anchors.top: renderSizeSpin.bottom
        anchors.leftMargin: 0
        horizontalAlignment: Text.AlignLeft
        anchors.rightMargin: 0
        font.weight: Font.Normal
        font.family: "Verdana"
        fontSizeMode: Text.Fit
        font.bold: false
        leftPadding: 8
        font.italic: false
        anchors.left: parent.left
        anchors.right: parent.right
        font.pointSize: 10
    }
    
    Label {
        id: charSettingsLabel
        x: 4
        y: -4
        text: qsTr("Characteristic")
        anchors.right: parent.right
        anchors.rightMargin: 0
        verticalAlignment: Text.AlignTop
        anchors.top: multiBatchCheck.bottom
        anchors.leftMargin: 0
        horizontalAlignment: Text.AlignHCenter
        font.weight: Font.Normal
        font.family: "Verdana"
        font.italic: false
        font.bold: true
        fontSizeMode: Text.Fit
        anchors.left: parent.left
        font.pointSize: 14
    }
    
    CheckBox {
        id: multiBatchCheck
        height: 28
        text: qsTr("Render multiple batches")
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: render3DCheck.bottom
        checkState: Qt.Unchecked
        
        onCheckStateChanged: {
            if(checked)
                multipleBatchesSeries.visible = true
            else
                multipleBatchesSeries.visible = false
            openglitem.renderMultipleBatches = checked
        }
    }

    Label {
        id: objectsTypeLabel
        x: -10
        y: 409
        text: qsTr("Objects type")
        anchors.right: parent.right
        font.family: "Verdana"
        anchors.leftMargin: 0
        verticalAlignment: Text.AlignTop
        fontSizeMode: Text.Fit
        leftPadding: 8
        anchors.left: parent.left
        font.bold: false
        font.weight: Font.Normal
        anchors.rightMargin: 0
        font.italic: false
        anchors.top: renderSettingsLabel.bottom
        horizontalAlignment: Text.AlignLeft
        font.pointSize: 10
    }

    ComboBox {
        id: objectsTypeCombo
        x: -8
        y: -115
        height: 28
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.right: parent.right
        textRole: "text"
        anchors.left: parent.left
        flat: false
        editable: false
        anchors.top: objectsTypeLabel.bottom
        currentIndex: 0
        model: ListModel {
            id: objectsTypeItems
            ListElement { text: "Points"; numverts: 1 }
            ListElement { text: "Triangles"; numverts: 3 }
            ListElement { text: "Quads"; numverts: 4 }
            ListElement { text: "5 triangles"; numverts: 15 }
        }

        Component.onCompleted: {
            openglitem.numObjectVertices = objectsTypeItems.get(currentIndex).numverts
        }
    }

    CheckBox {
        id: render3DCheck
        x: 0
        y: 430
        height: 28
        text: qsTr("Render 3D")
        checkable: true
        checked: false
        enabled: false
        anchors.rightMargin: 0
        checkState: Qt.Checked
        anchors.top: depthFuncCombo.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }

    Connections {
        target: renderCountMaxButton
        onClicked: {
            renderCountSpin.value = renderCountSpin.to
            chart.updateTitle(renderCountSpin.value)
        }
    }

    Connections {
        target: renderCountMinButton
        onClicked: {
            renderCountSpin.value = renderCountSpin.from
            chart.updateTitle(renderCountSpin.value)
        }
    }

    Connections {
        target: renderCountMedButton
        onClicked: {
            renderCountSpin.value = (renderCountSpin.to-renderCountSpin.from)/2
            chart.updateTitle(renderCountSpin.value)
        }
    }

    Connections {
        target: renderCountSpin
        onValueModified: chart.updateTitle(renderCountSpin.value)
    }

    Connections {
        target: charFirstText
        onEditingFinished: characteristicRangeSlider.first.value = charFirstText.text
    }

    Connections {
        target: charSecondText
        onEditingFinished: characteristicRangeSlider.second.value = charSecondText.text
    }

    Connections {
        target: generateCharButton
        onClicked: openglitem.generateCharacteristic(characteristicRangeSlider.first.value, characteristicRangeSlider.second.value, charStepSpin.value)
    }

    Connections {
        target: renderSizeSpin
        onValueModified: openglitem.renderSize = renderSizeSpin.value
    }

    Connections {
        target: depthFuncCombo
        onCurrentIndexChanged:  openglitem.depthFunc = depthFuncItems.get(depthFuncCombo.currentIndex).glfunc
    }

    Connections {
        target: objectsTypeCombo
        onCurrentIndexChanged:  openglitem.numObjectVertices = objectsTypeItems.get(objectsTypeCombo.currentIndex).numverts
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
