#include "openglrenderer.h"

#include <qopenglframebufferobject.h>
#include <qopenglextrafunctions.h>
#include <QTime>
#include <QElapsedTimer>
#include <QThread>
#include <QMutexLocker>
#include <QtMath>

OpenGLRenderer::OpenGLRenderer()
{
    QOpenGLExtraFunctions *ogl = QOpenGLContext::currentContext()->extraFunctions();

    m_RenderModel = std::make_unique<RenderModel>(3);

    const char *vertexShaderSource =
            "#version 400 core\n"
            "layout(location=0) in vec3 a_Position;\n"
            "layout(location=1) in vec3 a_Color;\n"
            "layout(location=2) in vec3 a_PosOffset;\n"
            "out vec3 v_Color;"
            "uniform int u_RenderSize;"
            "void main() {\n"
            " v_Color = a_Color;\n"
            " gl_PointSize = u_RenderSize;\n"
            " gl_Position = vec4((u_RenderSize * a_Position + a_PosOffset)/100.0, 1.0);\n"
            "}\n\0";

    const char *fragmentShaderSource =
            "#version 400 core\n"
            "in vec3 v_Color;\n"
            "out vec4 FragColor;\n"
            "void main() {\n"
            " FragColor = vec4(v_Color, 1.0);\n"
            "}\n\0";



    char infoLog[1024];

    m_ShaderProgram = ogl->glCreateProgram();

    GLuint vertexShader = ogl->glCreateShader(GL_VERTEX_SHADER);
    ogl->glShaderSource(vertexShader, 1, &vertexShaderSource, nullptr);
    ogl->glCompileShader(vertexShader);
    ogl->glAttachShader(m_ShaderProgram, vertexShader);
    ogl->glGetShaderInfoLog(vertexShader, 1024, nullptr, infoLog);
    qDebug() << infoLog;
    ogl->glDeleteShader(vertexShader);

    GLuint fragmentShader = ogl->glCreateShader(GL_FRAGMENT_SHADER);
    ogl->glShaderSource(fragmentShader, 1, &fragmentShaderSource, nullptr);
    ogl->glCompileShader(fragmentShader);
    ogl->glAttachShader(m_ShaderProgram, fragmentShader);
    ogl->glGetShaderInfoLog(fragmentShader, 1024, nullptr, infoLog);
    qDebug() << infoLog;
    ogl->glDeleteShader(fragmentShader);

    ogl->glLinkProgram(m_ShaderProgram);
    ogl->glGetProgramInfoLog(m_ShaderProgram, 1024, nullptr, infoLog);
    qDebug() << infoLog;

    ogl->glUseProgram(m_ShaderProgram);

    m_GpuTimer.setSampleCount(6);
    m_GpuTimer.create();
    m_GpuTimer.reset();
}

void OpenGLRenderer::render()
{
    int NUM_TRIANGLES = m_RenderCount;

    QOpenGLExtraFunctions *ogl = QOpenGLContext::currentContext()->extraFunctions();

    /* Create new render model if number of veritces has been changed. */
    if(numObjectVertices() != m_RenderModel->NUM_VERTICES_PER_OBJECT) {
        m_RenderModel = nullptr;
        m_RenderModel = std::make_unique<RenderModel>(m_NumObjectVertices);
    }

    if(depthFunc() == GL_FALSE)
        ogl->glDisable(GL_DEPTH_TEST);
    else {
        ogl->glEnable(GL_DEPTH_TEST);
        ogl->glDepthFunc(m_DepthFunc);
    }
    ogl->glDisable(GL_CULL_FACE);
    ogl->glDisable(GL_SCISSOR_TEST);
    ogl->glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    ogl->glDisable(GL_BLEND);
    ogl->glDisable(GL_TEXTURE_2D);
    ogl->glEnable(GL_PROGRAM_POINT_SIZE);

    ogl->glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
    ogl->glClearDepthf(1.0f);
    ogl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ogl->glUseProgram(m_ShaderProgram);
    ogl->glUniform1i(ogl->glGetUniformLocation(m_ShaderProgram, "u_RenderSize"), m_RenderSize);

    if(timeMeasureMode() == ETimeMeasureMode::eCountTimeCharacteristic)
    {
        /* Generate characteristic */
        static QXlsx::Document *excel = nullptr;
        static Chart *chart = nullptr;
        static int count_i = 0;
        static int count = 0;

        auto charRange = characteristicRange();
        auto step = characteristicStep();

        if(excel == nullptr)
        {
            /* Prepare excel file. */
            excel = new QXlsx::Document("characteristics.xlsx");
            excel->addSheet();
            excel->write(3, 1, "Rozmiar obiektu");
            excel->write(4, 1, m_RenderSize);
            excel->write(5, 1, "Test głębi");
            excel->write(6, 1, depthFunc());
            excel->write(3, 2, "Ilość obiektów");
            excel->write(3, 3, "Jeden batch");
            excel->write(3, 4, "Instancing");
            if(m_RenderMultipleBatches)
                excel->write(3, 5, "Wiele batchy");
            chart = excel->insertChart(4, 8, QSize(1280, 768));
            chart->setChartType(Chart::CT_ScatterChart);
            chart->setChartStyle(2);
            chart->setAxisTitle(Chart::ChartAxisPos::Left, "t [ms]");
            chart->setAxisTitle(Chart::ChartAxisPos::Bottom, "ilość obiektów");
            chart->setChartTitle("Czas renderowania w zależności od ilości obiektów");
            if(m_RenderMultipleBatches)
                chart->addSeries(CellRange(4,2, charRange.first+4, 5));
            else
                chart->addSeries(CellRange(4,2, charRange.first+4, 4));

            count_i = 0;
            count = charRange.first;
        }

        /* Measure render time. */
        renderAndMeasure(ogl, count);

        auto intervals = m_GpuTimer.waitForIntervals();
        auto samples = m_GpuTimer.waitForSamples();

        double multipleBatchesRenderInterval = double(intervals.at(0));
        double standardRenderInterval = double(intervals.at(2));
        double instancedRenderInterval = double(intervals.at(4));

        /* Write measurements to the excel. */
        excel->write(CellReference(count_i+4,2), count);
        excel->write(CellReference(count_i+4,3), standardRenderInterval/1000000.0);
        excel->write(CellReference(count_i+4,4), instancedRenderInterval/1000000.0);
        if(m_RenderMultipleBatches)
            excel->write(CellReference(count_i+4,5), multipleBatchesRenderInterval/1000000.0);

        /* Increment number of objects. */
        count_i++;
        count += step;

        if(count >= charRange.second) {
            /* Characteristic done. Save excel file and leave characteristic mode. */
            excel->saveAs("Characteristics.xlsx");
            delete excel;
            excel = nullptr;
            setTimeMeasureMode(ETimeMeasureMode::eLiveRenderTime);
        }
    }
    else
    {
        // live time

        renderAndMeasure(ogl, NUM_TRIANGLES);

        auto intervals = m_GpuTimer.waitForIntervals();
        auto samples = m_GpuTimer.waitForSamples();

        m_Mutex.lock();
        m_RenderTime.setX(samples.at(2)/1000000.0f);
        m_RenderTimeStamp.setX(intervals.at(2)/1000000.0f);
        m_RenderTime.setY(samples.at(4)/1000000.0f);
        m_RenderTimeStamp.setY(intervals.at(4)/1000000.0f);
        m_RenderTime.setZ(samples.at(0)/1000000.0f);
        m_RenderTimeStamp.setZ(intervals.at(0)/1000000.0f);
        m_Mutex.unlock();
    }

    if(m_Window)
        m_Window->resetOpenGLState();

    QQuickFramebufferObject::Renderer::update();
}

void OpenGLRenderer::renderAndMeasure(QOpenGLExtraFunctions *ogl, int numObjects)
{
    /* Get viewport set by Qt */
    GLint viewport[4];
    ogl->glGetIntegerv(GL_VIEWPORT, viewport);

    ogl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* Set left viewport */
    ogl->glViewport(viewport[0], viewport[1], viewport[2]/2, viewport[3]);

    m_GpuTimer.reset();

    /* Multiple batches render */
    if(m_RenderModel) {
        m_GpuTimer.recordSample();
        if(m_RenderMultipleBatches) {
            m_RenderModel->renderMultipleBatches(ogl, numObjects);
        }
        m_GpuTimer.recordSample();
    }

    ogl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* Set left viewport */
    ogl->glViewport(viewport[0], viewport[1], viewport[2]/2, viewport[3]);

    /* Single batch render */
    if(m_RenderModel) {
        m_GpuTimer.recordSample();
        m_RenderModel->renderSingleBatch(ogl, numObjects);
        m_GpuTimer.recordSample();
    }

    /* Set right viewport */
    ogl->glViewport(viewport[0]+viewport[2]/2, viewport[1], viewport[2]/2, viewport[3]);

     /* Instanced render */
    if(m_RenderModel) {
        m_GpuTimer.recordSample();
        m_RenderModel->renderInstanced(ogl, numObjects);
        m_GpuTimer.recordSample();
    }
}

QOpenGLFramebufferObject *OpenGLRenderer::createFramebufferObject(const QSize &size)
{
    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::Attachment::CombinedDepthStencil);
    return new QOpenGLFramebufferObject(size, format);
}

QVector4D OpenGLRenderer::getRenderTime()
{
    QMutexLocker locker(&m_Mutex);
    return m_RenderTime;
}

QVector4D OpenGLRenderer::getRenderTimeStamp()
{
    QMutexLocker locker(&m_Mutex);
    return m_RenderTimeStamp;
}

void OpenGLRenderer::setRenderCount(const int &count)
{
    QMutexLocker locker(&m_Mutex);
    m_RenderCount = count;
}

int OpenGLRenderer::renderCount() const
{
    QMutexLocker locker(&m_Mutex);
    return m_RenderCount;
}

void OpenGLRenderer::setRenderSize(const int &size)
{
    QMutexLocker locker(&m_Mutex);
    m_RenderSize = size;
}

int OpenGLRenderer::renderSize() const
{
    QMutexLocker locker(&m_Mutex);
    return m_RenderSize;
}

void OpenGLRenderer::setDepthFunc(const QString &func)
{
    QMutexLocker locker(&m_Mutex);
    m_DepthFunc = String2DepthFunc(func);
}

QString OpenGLRenderer::depthFunc()
{
    QMutexLocker locker(&m_Mutex);
    return DepthFunc2String(m_DepthFunc);
}

void OpenGLRenderer::setNumObjectVertices(const size_t &numVerts)
{
    QMutexLocker locker(&m_Mutex);
    m_NumObjectVertices = numVerts;
}

size_t OpenGLRenderer::numObjectVertices()
{
    QMutexLocker locker(&m_Mutex);
    return m_NumObjectVertices;
}

void OpenGLRenderer::setRenderMultipleBatches(bool state)
{
    QMutexLocker locker(&m_Mutex);
    m_RenderMultipleBatches = state;
}

bool OpenGLRenderer::renderMultipleBatches()
{
    QMutexLocker locker(&m_Mutex);
    return m_RenderMultipleBatches;
}

GLenum OpenGLRenderer::String2DepthFunc(const QString &func)
{
    if(func == "GL_LESS")
        return GL_LESS;
    else if(func == "GL_LEQUAL")
        return GL_LEQUAL;
    else
        return GL_FALSE;
}

QString OpenGLRenderer::DepthFunc2String(const GLenum &func)
{
    switch(func) {
    case GL_FALSE:
        return "DISABLED";

    case GL_LESS:
        return "GL_LESS";

    case GL_LEQUAL:
        return "GL_LEQUAL";

    default:
        return "UNKNOWN";
    }
}

int OpenGLRenderer::characteristicStep()
{
    QMutexLocker locker(&m_Mutex);
    return m_CharacteristicStep;
}

void OpenGLRenderer::setCharacteristicStep(int step)
{
    QMutexLocker locker(&m_Mutex);
    m_CharacteristicStep = step;
}

QPair<int, int> OpenGLRenderer::characteristicRange()
{
    return QPair<int, int>(m_CharacteristicStartValue, m_CharacteristicEndValue);
}

void OpenGLRenderer::setCharacteristicRange(int from, int to)
{
    QMutexLocker locker(&m_Mutex);
    m_CharacteristicStartValue = from;
    m_CharacteristicEndValue = to;
}

void OpenGLRenderer::generateCharacteristic(int from, int to, int step)
{
    setCharacteristicRange(from, to);
    setCharacteristicStep(step);
    setTimeMeasureMode(ETimeMeasureMode::eCountTimeCharacteristic);
}

void OpenGLRenderer::setTimeMeasureMode(OpenGLRenderer::ETimeMeasureMode mode)
{
    QMutexLocker locker(&m_Mutex);
    m_MeasureMode = mode;
}

OpenGLRenderer::ETimeMeasureMode OpenGLRenderer::timeMeasureMode()
{
    return m_MeasureMode;
}
