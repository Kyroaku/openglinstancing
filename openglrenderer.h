#ifndef OPENGLRENDERER_H
#define OPENGLRENDERER_H

#include <QMutex>
#include <QOpenGLTimeMonitor>
#include <qopenglfunctions.h>
#include <qopenglvertexarrayobject.h>
#include <qquickframebufferobject.h>
#include <qquickwindow.h>

#include "xlsxdocument.h"
#include "xlsxchartsheet.h"
#include "xlsxcellrange.h"
#include "xlsxchart.h"
#include "xlsxrichstring.h"
#include "xlsxworkbook.h"

#include "rendermodel.h"


using namespace QXlsx;


class OpenGLRenderer : public QQuickFramebufferObject::Renderer
{
public:
    enum ETimeMeasureMode {
        eLiveRenderTime,
        eCountTimeCharacteristic
    };

private:
    GLuint m_ShaderProgram;
    QQuickWindow *m_Window = nullptr;
    QVector4D m_RenderTime;
    QVector4D m_RenderTimeStamp;
    mutable QMutex m_Mutex;
    int m_RenderCount = 3;
    int m_RenderSize = 1;
    GLenum m_DepthFunc = GL_FALSE;
    size_t m_NumObjectVertices = 1;
    bool m_RenderMultipleBatches = false;

    std::unique_ptr<RenderModel> m_RenderModel;

    ETimeMeasureMode m_MeasureMode = ETimeMeasureMode::eLiveRenderTime;
    int m_CharacteristicStartValue;
    int m_CharacteristicEndValue;
    int m_CharacteristicStep;
    QOpenGLTimeMonitor m_GpuTimer;

public:
    OpenGLRenderer();

    void render();
    void renderAndMeasure(QOpenGLExtraFunctions *ogl, int numObjects);

    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size);

    QVector4D getRenderTime();
    QVector4D getRenderTimeStamp();

    void setRenderCount(const int &count);
    int renderCount() const;

    void setRenderSize(const int &count);
    int renderSize() const;

    void setDepthFunc(const QString &func);
    QString depthFunc();

    void setNumObjectVertices(const size_t &numVerts);
    size_t numObjectVertices();

    void setRenderMultipleBatches(bool state);
    bool renderMultipleBatches();

    GLenum String2DepthFunc(const QString &func);
    QString DepthFunc2String(const GLenum &func);

    int characteristicStep();
    void setCharacteristicStep(int step);
    QPair<int, int> characteristicRange();
    void setCharacteristicRange(int from, int to);
    void generateCharacteristic(int from, int to, int step);
    void setTimeMeasureMode(ETimeMeasureMode mode);
    ETimeMeasureMode timeMeasureMode();

};

#endif // OPENGLRENDERER_H
