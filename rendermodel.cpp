#include "rendermodel.h"

#include <qopenglextrafunctions.h>

#include <qdebug.h>

RenderModel::RenderModel(const size_t &numVertices)
    : NUM_VERTICES_PER_OBJECT(numVertices)
{
    switch(numVertices) {
    case 1: m_RenderMode = GL_POINTS; break;
    case 3: m_RenderMode = GL_TRIANGLES; break;
    case 4: m_RenderMode = GL_QUADS; break;
    default: m_RenderMode = GL_TRIANGLES; break;
    }

    QOpenGLExtraFunctions *ogl = QOpenGLContext::currentContext()->extraFunctions();
    generateData(ogl);
}

RenderModel::~RenderModel()
{
    auto ogl = QOpenGLContext::currentContext()->extraFunctions();
    ogl->glDeleteBuffers(1, &m_VBO_pos);
    ogl->glDeleteBuffers(1, &m_VBO_verts);
    ogl->glDeleteBuffers(1, &m_VBO_colors);
    ogl->glDeleteBuffers(1, &m_VBO_pos_inst);
    ogl->glDeleteBuffers(1, &m_VBO_verts_inst);
    ogl->glDeleteBuffers(1, &m_VBO_colors_inst);
    m_VAO->destroy();
    m_VAO_inst->destroy();
    qDebug() << "Destroyed";
}

void RenderModel::renderMultipleBatches(QOpenGLExtraFunctions *ogl, int numObjects)
{
    m_VAO_inst->bind();
    for(int i = 0; i < numObjects; i++)
        ogl->glDrawArrays(m_RenderMode, 0, GLsizei(NUM_VERTICES_PER_OBJECT));
    m_VAO_inst->release();
}

void RenderModel::renderSingleBatch(QOpenGLExtraFunctions *ogl, int numObjects)
{
    m_VAO->bind();
    ogl->glDrawArrays(m_RenderMode, 0, numObjects * GLsizei(NUM_VERTICES_PER_OBJECT));
    m_VAO->release();
}

void RenderModel::renderInstanced(QOpenGLExtraFunctions *ogl, int numObjects)
{
    m_VAO_inst->bind();
    ogl->glDrawArraysInstanced(m_RenderMode, 0, GLsizei(NUM_VERTICES_PER_OBJECT), numObjects);
    m_VAO_inst->release();
}

void RenderModel::generateData(QOpenGLExtraFunctions *ogl)
{
    /* single batch data */
    auto verts = std::make_unique<float[]>(MAX_OBJECTS * NUM_VERTICES_PER_OBJECT * 3);
    auto positions = std::make_unique<float[]>(MAX_OBJECTS * NUM_VERTICES_PER_OBJECT * 3);
    auto colors = std::make_unique<float[]>(MAX_OBJECTS * NUM_VERTICES_PER_OBJECT * 3);

    /* instanced data */
    auto verts_inst = std::make_unique<float[]>(1 * NUM_VERTICES_PER_OBJECT * 3);
    auto positions_inst = std::make_unique<float[]>(MAX_OBJECTS * 1 * 3);
    auto colors_inst = std::make_unique<float[]>(MAX_OBJECTS * 1 * 3);

    /* instanced data */
    if(NUM_VERTICES_PER_OBJECT <= 4 && NUM_VERTICES_PER_OBJECT > 1) {
        /* Triangles and quads */
        for(size_t i = 0; i < NUM_VERTICES_PER_OBJECT; i++) {
            verts_inst[i*3 + 0] = cosf(2.0f * 3.141527f * float(i)/NUM_VERTICES_PER_OBJECT);
            verts_inst[i*3 + 1] = sinf(2.0f * 3.141527f * float(i)/NUM_VERTICES_PER_OBJECT);
            verts_inst[i*3 + 2] = 0.0f;
        }
    } else {
        /* Circles built by triangles or points*/
        for(size_t i = 0; i < NUM_VERTICES_PER_OBJECT; i++) {
            switch(i%3) {
            case 0:
                verts_inst[i*3 + 0] = 0.0f;
                verts_inst[i*3 + 1] = 0.0f;
                verts_inst[i*3 + 2] = 0.0f;
                break;
            case 1:
                verts_inst[i*3 + 0] = cosf(2.0f * 3.141527f * float(i/3)/(NUM_VERTICES_PER_OBJECT/3));
                verts_inst[i*3 + 1] = sinf(2.0f * 3.141527f * float(i/3)/(NUM_VERTICES_PER_OBJECT/3));
                verts_inst[i*3 + 2] = 0.0f;
                break;
            case 2:
                verts_inst[i*3 + 0] = cosf(2.0f * 3.141527f * float(i/3+1)/(NUM_VERTICES_PER_OBJECT/3));
                verts_inst[i*3 + 1] = sinf(2.0f * 3.141527f * float(i/3+1)/(NUM_VERTICES_PER_OBJECT/3));
                verts_inst[i*3 + 2] = 0.0f;
                break;
            }
        }
    }

    for(size_t i = 0; i < MAX_OBJECTS; i++) {
        float x = float(rand()) / RAND_MAX * 200.0f - 100.0f;
        float y = float(rand()) / RAND_MAX * 200.0f - 100.0f;
        float z = float(rand()) / RAND_MAX * 2.0f - 1.0f;
        float r = float(rand()) / RAND_MAX;
        float g = float(rand()) / RAND_MAX;
        float b = float(rand()) / RAND_MAX;

        /* single batch data */
        if(NUM_VERTICES_PER_OBJECT <= 4 && NUM_VERTICES_PER_OBJECT > 1) {
            /* Triangles and quads */
            for(size_t j = 0; j < NUM_VERTICES_PER_OBJECT; j++) {
                verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 0] = cosf(2.0f * 3.141527f * float(j)/NUM_VERTICES_PER_OBJECT);
                verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 1] = sinf(2.0f * 3.141527f * float(j)/NUM_VERTICES_PER_OBJECT);
                verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 2] = 0.0f;

                positions[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 0] = x;
                positions[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 1] = y;
                positions[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 2] = z;

                colors[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 0] = r;
                colors[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 1] = g;
                colors[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 2] = b;
            }
        } else {
            /* Circles built by triangles or points */
            for(size_t j = 0; j < NUM_VERTICES_PER_OBJECT; j++) {
                switch(j%3) {
                case 0:
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 0] = 0.0f;
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 1] = 0.0f;
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 2] = 0.0f;
                    break;
                case 1:
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 0] = cosf(2.0f * 3.141527f * float(j/3)/(NUM_VERTICES_PER_OBJECT/3));
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 1] = sinf(2.0f * 3.141527f * float(j/3)/(NUM_VERTICES_PER_OBJECT/3));
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 2] = 0.0f;
                    break;
                case 2:
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 0] = cosf(2.0f * 3.141527f * float(j/3+1)/(NUM_VERTICES_PER_OBJECT/3));
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 1] = sinf(2.0f * 3.141527f * float(j/3+1)/(NUM_VERTICES_PER_OBJECT/3));
                    verts[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 2] = 0.0f;
                    break;
                }

                positions[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 0] = x;
                positions[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 1] = y;
                positions[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 2] = z;

                colors[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 0] = r;
                colors[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 1] = g;
                colors[i*NUM_VERTICES_PER_OBJECT*3 + j*3 + 2] = b;
            }
        }

        /* instanced data */
        positions_inst[i*3 + 0] = x;
        positions_inst[i*3 + 1] = y;
        positions_inst[i*3 + 2] = z;

        colors_inst[i*3 + 0] = r;
        colors_inst[i*3 + 1] = g;
        colors_inst[i*3 + 2] = b;
    }

    /* Standard GPU data */
    m_VAO = std::make_unique<QOpenGLVertexArrayObject>();
    m_VAO->create();
    m_VAO->bind();

    ogl->glGenBuffers(1, &m_VBO_verts);
    ogl->glBindBuffer(GL_ARRAY_BUFFER, m_VBO_verts);
    ogl->glBufferData(GL_ARRAY_BUFFER, GLsizei(MAX_OBJECTS * NUM_VERTICES_PER_OBJECT * 3 * sizeof(float)), verts.get(), GL_STATIC_DRAW);
    ogl->glEnableVertexAttribArray(0);
    ogl->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    ogl->glGenBuffers(1, &m_VBO_colors);
    ogl->glBindBuffer(GL_ARRAY_BUFFER, m_VBO_colors);
    ogl->glBufferData(GL_ARRAY_BUFFER, GLsizei(MAX_OBJECTS * NUM_VERTICES_PER_OBJECT * 3 * sizeof(float)), colors.get(), GL_STATIC_DRAW);
    ogl->glEnableVertexAttribArray(1);
    ogl->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    ogl->glGenBuffers(1, &m_VBO_pos);
    ogl->glBindBuffer(GL_ARRAY_BUFFER, m_VBO_pos);
    ogl->glBufferData(GL_ARRAY_BUFFER, GLsizei(MAX_OBJECTS * NUM_VERTICES_PER_OBJECT * 3 * sizeof(float)), positions.get(), GL_STATIC_DRAW);
    ogl->glEnableVertexAttribArray(2);
    ogl->glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    ogl->glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_VAO->release();

    /* Instanced GPU data */
    m_VAO_inst = std::make_unique<QOpenGLVertexArrayObject>();
    m_VAO_inst->create();
    m_VAO_inst->bind();

    ogl->glGenBuffers(1, &m_VBO_verts_inst);
    ogl->glBindBuffer(GL_ARRAY_BUFFER, m_VBO_verts_inst);
    ogl->glBufferData(GL_ARRAY_BUFFER, GLsizei(1 * NUM_VERTICES_PER_OBJECT * 3 * sizeof(float)), verts_inst.get(), GL_STATIC_DRAW);
    ogl->glEnableVertexAttribArray(0);
    ogl->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    ogl->glGenBuffers(1, &m_VBO_colors_inst);
    ogl->glBindBuffer(GL_ARRAY_BUFFER, m_VBO_colors_inst);
    ogl->glBufferData(GL_ARRAY_BUFFER, GLsizei(MAX_OBJECTS * 3 * sizeof(float)), colors_inst.get(), GL_STATIC_DRAW);
    ogl->glEnableVertexAttribArray(1);
    ogl->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    ogl->glGenBuffers(1, &m_VBO_pos_inst);
    ogl->glBindBuffer(GL_ARRAY_BUFFER, m_VBO_pos_inst);
    ogl->glBufferData(GL_ARRAY_BUFFER, GLsizei(MAX_OBJECTS * 3 * sizeof(float)), positions_inst.get(), GL_STATIC_DRAW);
    ogl->glEnableVertexAttribArray(2);
    ogl->glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    ogl->glBindBuffer(GL_ARRAY_BUFFER, 0);

    ogl->glVertexAttribDivisor(0, 0); // vertex position per vertex
    ogl->glVertexAttribDivisor(1, 1); // color per instance
    ogl->glVertexAttribDivisor(2, 1); // triangle position per instance

    m_VAO_inst->release();
}
